using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] float crashDelay = 1.0f;
    [SerializeField] float loadNextLevelDelay = 1.0f;
    [SerializeField] AudioClip audioLevelSucced;
    [SerializeField] AudioClip audioCrash;

    [SerializeField] ParticleSystem succesParticles;
    [SerializeField] ParticleSystem crashedParticles;

    AudioSource audioSource;

    bool isTransitioning = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    void OnCollisionEnter(Collision collision)
    {
        if (isTransitioning || getPlayerTag() == "God") { return; }
        
        switch(collision.gameObject.tag)
        {
            case "Friendly":
                Debug.Log("Hello Friend !");
                break;

            case "Finish":
                StartLoadNextLevelSequence();
                break;

            default:
                StartCrashSequence();
                break;
        }        
    }

    public string getPlayerTag()
    {
        return gameObject.tag;
    }
    void StartCrashSequence()
    {
        isTransitioning = true;
        DiablePlayerMovement();
        PlaySoundEffect(audioCrash);
        PlayParticule(crashedParticles);
        Invoke("ReloadLevel", crashDelay);
        
    }

    void StartLoadNextLevelSequence()
    {
        isTransitioning = true;
        DiablePlayerMovement();
        PlaySoundEffect(audioLevelSucced);
        PlayParticule(succesParticles);
        Invoke("LoadNextLevel", loadNextLevelDelay);
    }

    void ReloadLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        int nextSceneIndex = (currentSceneIndex + 1) % SceneManager.sceneCountInBuildSettings;

        SceneManager.LoadScene(nextSceneIndex);
    }

    void DiablePlayerMovement()
    {
        GetComponent<Movement>().enabled = false;
    }

    void PlaySoundEffect(AudioClip audioSound)
    {
        audioSource.Stop();
        audioSource.PlayOneShot(audioSound);
    }

    void PlayParticule(ParticleSystem particleSystem)
    {
        particleSystem.Play();
    }
}
