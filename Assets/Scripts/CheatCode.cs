using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheatCode : MonoBehaviour
{
    BoxCollider playerCollider;
    CollisionHandler collisionHandler;
    void Start()
    {
        playerCollider = GetComponent<BoxCollider>();
        collisionHandler = FindObjectOfType<CollisionHandler>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }
        else if(Input.GetKeyDown(KeyCode.C)) 
        {
            ToggleCollision();
        }
    }

    void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        int nextSceneIndex = (currentSceneIndex + 1) % SceneManager.sceneCountInBuildSettings;

        SceneManager.LoadScene(nextSceneIndex);
    }

    void ToggleCollision()
    {
        if (collisionHandler != null) 
        {
            string playerTag = collisionHandler.getPlayerTag(); 
            if(playerTag == "Player")
            {
                gameObject.tag = "God";
            }
            else
            {
                gameObject.tag = "Player";
            }
        }
    }
}
