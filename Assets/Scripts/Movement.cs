using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float boosterSpeed = 1.0f;
    [SerializeField] float rotationSpeed = 1.0f;
    [SerializeField] AudioClip rocketSound;
    [SerializeField] ParticleSystem mainBoosterParticle;
    [SerializeField] ParticleSystem leftBoosterParticle;
    [SerializeField] ParticleSystem rightBoosterParticle;

    Rigidbody myRigidbody;
    AudioSource myAudioSource;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
        myAudioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        ProcessThrust();
        ProcessRotation();
    }

    void ProcessThrust()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            StartThrust();
        }
        else
        {
            StopThrust();
        }
    }

    void StartThrust()
    {
        ActivateParticles(mainBoosterParticle, true);
        PlayRocketSound(true);
        myRigidbody.AddRelativeForce(Vector3.up * boosterSpeed * Time.deltaTime);
    }

    void StopThrust()
    {
        ActivateParticles(mainBoosterParticle, false);
        PlayRocketSound(false);
    }

    void ProcessRotation()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Rotate(1);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            Rotate(-1);
        }
        else
        {
            DeactivateSideBoosters();
        }
    }

    void Rotate(float direction)
    {
        myRigidbody.freezeRotation = true; // Freeze rotation so we can manually rotate
        transform.Rotate(Vector3.forward * direction * rotationSpeed * Time.deltaTime);
        myRigidbody.freezeRotation = false; // Unfreeze rotation so physics system can take over

        ActivateParticles(direction > 0 ? rightBoosterParticle : leftBoosterParticle, true);
    }

    void DeactivateSideBoosters()
    {
        ActivateParticles(leftBoosterParticle, false);
        ActivateParticles(rightBoosterParticle, false);
    }

    void PlayRocketSound(bool shouldPlay)
    {
        if (shouldPlay && !myAudioSource.isPlaying)
        {
            myAudioSource.PlayOneShot(rocketSound);
        }
        else if (!shouldPlay && myAudioSource.isPlaying)
        {
            myAudioSource.Stop();
        }
    }

    void ActivateParticles(ParticleSystem particles, bool activate)
    {
        if (activate)
        {
            if (!particles.isPlaying)
            {
                particles.Play();
            }
        }
        else if (particles.isPlaying)
        {
            particles.Stop();
        }
    }
}
